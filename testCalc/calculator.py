from sys import stderr


class Calculator():
    def __init__(self, module, name, session):
        self.session = session
        self.module = module
        self.name = name
        self.values = {}

    def load(self):
        self.definition = self.session.request(
            endPoint=f"/module/{self.module}/calculator/{self.name}", http='GET', verbose=False)
        self.dims = [dim for dim in self.definition['defaults']['dimensions']]
        self.tables = [req['table'] for req in self.definition['requests']]
        self.distributionAxis = self.definition['distribution']['distributionAxis']
        try:
            self.metrics = self.definition['parameters'].get('metrics', [])
        except:
            print(
                f"[{self.name}] calculator: bad syntax for parameters {self.definition.get('parameters', '')}", file=stderr)
            self.metrics = []
        self.check_definition()

    def check_definition(self):
        self.failedChecks = 0
        # 1- Calculator requests where the distribution axis is not in the set of dimensions to group by must have forceNonDistributed set to true
        for req in self.definition['requests']:
            if req['distribute'] == True:
                if not self.distributionAxis in req['by']:
                    print(
                        f"[{self.name}]: A raw request should have the distribution axis in its 'By' dimensions list !", file=stderr)
                    self.failedChecks += 1

    def getValues(self, dim):
        if not dim in self.values:
            self.values[dim] = self.session.request(f"/module/{self.module}/values/{dim}",
                                                    http='GET', verbose=False
                                                    )['values']
        return self.values.get(dim)

    def dimByRandom(self, rng, nb=None):
        nb = nb if nb else rng.integers(len(self.dims)+1)
        return list(rng.choice(self.dims, nb, replace=False))

    def metricRandom(self, rng, nb=None):
        nb = nb if nb else rng.integers(len(self.metrics)+1)
        return list(rng.choice(self.metrics, nb, replace=False))

    def calculateRequestBody(self, dimBy=None, where=None, metrics=None, branch='official'):
        dimBy = dimBy if dimBy else self.definition['defaults']['cols'] + \
            self.definition['defaults']['rows']
        parameters = self.definition.get('parameters')
        if dimBy:
            parameters['pivot_fields'] = {
                'rows': dimBy[:-1], 'columns': [dimBy[-1]]} if dimBy else {'rows': [], 'columns': []}
        if metrics:
            parameters['metrics'] = metrics
        where = where if where else self.definition['defaults']['where']

        versioning = {}
        for table in self.tables:
            result = self.session.branches(
                table, output='versioning', name=branch, verbose=False)
            if result:
                versioning[table] = result

        return {
            "calculatorName": self.name,
            "parameters": parameters,
            "by": dimBy,
            "where": where,
            "versioning": versioning
        }

    def to_csv(self):
        record = dict(name=self.name,
                      module=self.module,
                      metricsNb=len(self.metrics),
                      nbDims=len(self.dims),
                      sources=self.tables,
                      distributedOn=self.definition['distribution']['distributionAxis'],
                      failedCheck=self.failedChecks
                      )
        return record
