import pandas as pd
from pathlib import Path

pathdir = Path('output')
global_report = Path('global/global_report.csv')

result = pd.concat((pd.read_csv(file
                                ).assign(server=file.stem.split('_')[0],
                                         date=pd.to_datetime(
                                             file.stem.split('_')[1]),
                                        version= file.parts[1],
                                         )
                    for file in pathdir.glob('**/*.csv', )),
                   ignore_index=True).drop(columns= 'Unnamed: 0')

result.to_csv(global_report, index= False)
