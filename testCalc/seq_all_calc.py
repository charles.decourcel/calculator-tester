from sys import stderr
from project import currentSession
from calculator import Calculator
from reqSequence import ReqSequence
import argparse
from numpy.random import default_rng


parser = argparse.ArgumentParser()
parser.add_argument('-c', '--calculator', default='',
                    nargs='?', help='calculator name')
parser.add_argument('-o', '--output', default=None, nargs='?',
                    help='filepath of the calculator requests calls')
parser.add_argument('-s', '--seed', default=None, nargs='?',
                    help='Specify a seed for numpy random generator')
parser.add_argument('--nb', default=2, nargs='?',
                    help='In case of a specific calculator, the number of requests to generate')

args = parser.parse_args()

if __name__ == '__main__':
    seed = int(args.seed) if args.seed else 456
    print(f"Seed = {seed}", file=stderr)
    rng = default_rng(seed)

    calcs_info = []

    session = currentSession()
    filepath = args.output if args.output else f'calculatorSeq.json'
    calcSeq = ReqSequence(filepath)

    modules = session.request(endPoint=f"/modules", http='GET', verbose=False)
    for module in modules:
        # for module in ['sensi']:
        names = [calc_def['name'] for calc_def in session.request(
            endPoint=f"/module/{module}/calculator", http='GET', verbose=False)]
        if args.calculator in names:
            calc_names = [args.calculator]
        elif not args.calculator:
            calc_names = names
        else:
            continue

        #      if args.calculator else None
        # if not calc_names:
        #     calc_names= [calc_def['name'] for calc_def in session.request(endPoint= f"/module/{module}/calculator", http= 'GET', verbose= False)]

        calcs = [Calculator(module=module, name=name, session=session)
                 for name in calc_names]
        for calc in calcs:
            calc.load()
            if args.calculator:
                for i in range(int(args.nb)):
                    calcSeq.add(endPoint=f"/module/{module}/calculate",
                                httpMethod='POST', body=calc.calculateRequestBody(dimBy=calc.dimByRandom(rng), metrics=calc.metricRandom(rng)))
            else:
                calcSeq.add(endPoint=f"/module/{module}/calculate",
                            httpMethod='POST', body=calc.calculateRequestBody())
            print(module, calc.name)
        calcs_info.extend([calc.to_csv() for calc in calcs])
    calcSeq.save(url=session.url, user=session.user)

    # df_info= pd.DataFrame(calcs_info)
    # df_info.to_csv('output/calcStats.csv')

    # print(df_info)
