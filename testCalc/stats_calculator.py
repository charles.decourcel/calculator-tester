from calculator import Calculator
from reqSequence import ReqSequence
from project import currentSession, stderr
import pandas as pd
import json
import argparse


parser = argparse.ArgumentParser()
parser.add_argument(
    'file', help='A json file with a sequence of Calls for calculators')
parser.add_argument(
    '--filter',
    help='list of integers representing index of requests to filter in (only those requests are processed). Example: --filter "0,3,5"'
)

if __name__ == '__main__':
    args = parser.parse_args()
    filt = [int(i) for i in args.filter.split(',')] if args.filter else None
    calcSeq = ReqSequence(args.file)
    calcSeq.load()
    session = currentSession()
    allStats = []
    for reqSeq in calcSeq.reqs:
        if ('endPoint' in reqSeq) and ('body' in reqSeq) and (not filt or reqSeq['#'] in filt):
            print(reqSeq['endPoint'], file=stderr)
            reqObj = json.loads(reqSeq['body'])
            if not 'calculatorName' in reqObj:
                continue
            module = reqSeq['endPoint'].split('/')[2]
            name = reqObj['calculatorName']
            try:
                metrics = reqObj['parameters'].get('metrics', [])
            except:
                metrics = []
            dims = reqObj['by']
            myCalc = Calculator(module=module, name=name, session=session)
            myCalc.load()
            stats = myCalc.to_csv()
            stats['MetricsOk'] = False
            stats['DimsOk'] = False
            stats['SchemaOk'] = False
            if 'fields' in reqSeq:
                if all([metric in reqSeq['fields'] for metric in metrics]):
                    stats['MetricsOk'] = True
                if all([dim in reqSeq['fields'] for dim in dims]):
                    stats['DimsOk'] = True
                stats['SchemaOk'] = set(dims+metrics) == set(reqSeq['fields'])
            stats['Status'] = reqSeq.get('status', None)
            stats['Error'] = 'error' in reqSeq
            try:
                error= json.loads(reqSeq['error']) if 'error' in reqSeq else None
                stats['ErrorType'] = error['rafalError'] if error else None
            except:
                stats['ErrorType']= reqSeq['error'].split(' ')[1]
            stats['recordsNb'] = reqSeq.get('recordsNb', 0)
            stats['duration'] = reqSeq.get('duration', None)
            allStats.append(stats)

            # print(stats)
    df = pd.DataFrame(allStats)
    print(df,file=stderr)
    print(df.to_csv())
