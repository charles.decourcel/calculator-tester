from notionio import Database
from pathlib import Path
import pandas as pd
from os import getenv

def str_to_list(listString):
    return listString.replace('\'', '').strip('][').split(', ')

def duration(dur):
    return round(float(dur),2)

if __name__ == '__main__':
    pageId= 'b4e1989577f94b4cb37eb36ab7b56285'
    dbId= '368574e1ccec415087909bde6fae2589'
    db= Database(dbId, auth= getenv('NOTION_KEY', ''))
    db.load()

    source= '/home/chcdc/tp/testOpc/global/global_report.csv'
    df= pd.read_csv(source, index_col=None, quotechar='"', converters= {'sources': str_to_list, 'duration': duration})

    for i, row in enumerate(df.to_dict(orient='records')):
        print(f"{i:03d}/{len(df)}", row['server'], row['date'], row['module'], row['name'])
        children= [('bulleted_list_item', f"{fld} : '{val}'") for fld, val in row.items()] 
        db.createPage(**row, children= children)


    