from project import currentSession, module
from reqSequence import ReqSequence
import argparse
from numpy.random import default_rng
import pandas as pd

parser= argparse.ArgumentParser()
parser.add_argument('calculator', default= None, nargs='?', help='calculator name')
parser.add_argument('-n', '--nb', default= 5, help='nb of queries to test')
parser.add_argument('-s', '--seed', default= None, nargs='?', help='Specify a seed for numpy random generator')
parser.add_argument('-f', '--file', default= None, nargs='?', help='filepath of the calculator request calls')
args = parser.parse_args()

class Calculator():
    def __init__(self, module, name, session):
        self.session = session
        self.module = module
        self.name= name
        self.values= {}
    
    def load(self):
        self.definition = self.session.request(endPoint= f"/module/{self.module}/calculator/{self.name}", http= 'GET', verbose= False)
        self.dims= [dim for dim in self.definition['defaults']['dimensions']]
        self.tables= [req['table'] for req in self.definition['requests']]
        self.distributionAxis= self.definition['distribution']['distributionAxis']
        try:
            self.metrics= self.definition['parameters'].get('metrics', [])
        except:
            print(f"[{self.name}] calculator: bad syntax for parameters {self.definition.get('parameters', '')}")
            self.metrics= []
        self.check_definition()

    def check_definition(self):
        self.failedChecks= 0
        # 1- Calculator requests where the distribution axis is not in the set of dimensions to group by must have forceNonDistributed set to true
        for req in self.definition['requests']:
            if req['distribute'] == True:
                if not self.distributionAxis in req['by']:
                    print(f"[{self.name}]: A raw request should have the distribution axis in its 'By' dimensions list !")
                    self.failedChecks += 1

    def getValues(self, dim):
        if not dim in self.values:
            self.values[dim]= self.session.request(f"/module/{self.module}/values/{dim}", 
                                                    http= 'GET', verbose = False
                                                    )['values']
        return self.values.get(dim)

    def calculateRequestBody(self, dimBy= None, where= None, metrics= None, branch= 'official'):
        dimBy= dimBy if dimBy else self.definition['defaults']['cols'] + self.definition['defaults']['rows']
        parameters= self.definition.get('parameters')
        if dimBy:
            parameters['pivot_fields']= {'rows': dimBy[:-1], 'columns': [dimBy[-1]]} if dimBy else {'rows': [], 'columns': []}
        if metrics:
            parameters['metrics']= metrics
        where= where if where else self.definition['defaults']['where']

        versioning= {}
        for table in self.tables:
            result= self.session.branches(table, output= 'versioning', name= branch, verbose= False)
            if result:
                versioning[table]= result

        return {
            "calculatorName": self.name,
            "parameters": parameters,
            "by": dimBy,
            "where": where,
            "versioning": versioning
        }

    def to_csv(self):
        record= dict(name= self.name,
                    module=module,
                    metricsNb= len(self.metrics),
                    nbDims= len(self.dims),
                    # sourceNb= len(self.definition['requests']),
                    sources= self.tables,
                    distributedOn= self.definition['distribution']['distributionAxis'],
                    failedCheck= self.failedChecks
        )
        return record


if __name__ == '__main__':
    seed= int(args.seed) if args.seed else 456
    print(f"Seed = {seed}")
    rng = default_rng(seed)

    calcs_info= []

    session= currentSession()
    filepath= args.file if args.file else f'output/calculatorSeq.json'
    calcSeq= ReqSequence(filepath)

    modules= session.request(endPoint= f"/modules", http= 'GET', verbose= False)
    for module in modules:
    # for module in ['sensi']:
        calc_names= [args.calculator] if args.calculator else None
        if not calc_names:
            calc_names= [calc_def['name'] for calc_def in session.request(endPoint= f"/module/{module}/calculator", http= 'GET', verbose= False)]

        calcs= [Calculator(module= module, name= name, session= session) for name in calc_names]
        for calc in calcs:
            calc.load()
            calcSeq.add(endPoint= f"/module/{module}/calculate", httpMethod= 'POST', body= calc.calculateRequestBody())
            print(module, calc.name)
        calcs_info.extend([calc.to_csv() for calc in calcs])
    calcSeq.save(url= session.url, user= session.user)
        
    df_info= pd.DataFrame(calcs_info)
    df_info.to_csv('output/calcStats.csv')

    print(df_info)

    raise SystemExit

    byLen=3
    byIncl=[]
    whereLen= 1
    branch='official'

    self= Calculator(module= 'sensi', name= 'FRTB_GIRR_Sensi', session= session)
    self.load()

    dims, halfDimNb= self.dims, len(self.dims)//2
    dimBy= list(rng.choice(dims, min(byLen, halfDimNb), replace= False))
    # pivot_fields= {'rows': dimBy[:-1], 'columns': [dimBy[-1]]} if dimBy else {'rows': [], 'columns': []}
    if byIncl:
        dimBy.extend(byIncl)
    dimValues= list(rng.choice(list(set(dims).difference(dimBy)), min(whereLen, halfDimNb), replace= False))
    values= {dim: self.getValues(dim) for dim in dimValues}
    whereDims= list(values.keys())
    where= {dim: {'values': list(rng.choice(values[dim], 2, replace= True))} for dim in whereDims}

    calculateRequestBody= self.calculateRequestBody()
    print(calculateRequestBody)
    result= session.request(endPoint= f"/module/{module}/calculate", jsonText= calculateRequestBody, output='text', verbose= False)
    if 'rafalError' in result:
        print(result)
    else:
        df= pd.read_json(result, orient='records',lines=True)
        print(df)
        
