#!/bin/bash
version_short=$(omb info | awk '/version/ {gsub(/"/,"");gsub(/,/,"");print $2}' | cut -c1-8)
today="$(date '+%F')"
report_dir="${1:-output}/${version_short}/${server}/${today}"
file="${report_dir}/${server}_${today}_calcs.json"
report="${report_dir}/${server}_${today}_calcs_report.json"
csv="${report_dir}/${server}_${today}_calcs_stats.csv"

# Create test directory
mkdir -p "${report_dir}"

# Create and write the sequence of Calculateur call , for all calculators
python testCalc/seq_all_calc.py -o ${file}

# We shift parameters to send all parameter to omb reqs replay except the first one
shift 
# run the sequence of API calls and save a report
omb reqs replay --elapse 1 ${file} $*

# From the report of API calls, compute some stats in csv
python testCalc/stats_calculator.py ${report} > ${csv}

# Concat this report with prvious reports into an aggregated report file
python testCalc/concat_csv.py 