# calculator-tester

## Test all calculators

To run automated testing on every calculator with default parameters (by default asynchronously, with 1 sec laps between calls):

```bash
. run.sh
```

To run tests on every calculator sequentially on some API call only (on request #4, #5 and #10) :

```bash
. run.sh "" --synchronous --filter "4,5,10"
```

To run tests on every calculator asynchronously with 5 seconds laps between calls : 

```bash
. run.sh "" --elapse 5
```

## Test one calculator

To run some random test on a given calculator : 

```bash
. runRandom.sh "<calc_name>"
```

To run 12 random tests on calculator *MetricsV1* with a given seed : 

```bash
. runRandom.sh "MetricsV1" "" --nb 12
```

## Requirement

requires :

- data-model-builder
- rafal-driver
