# Testing of reference calculators for v5.4, with default parameters

In theses tests, we get all calculator definition and prepare requests using the default parameters, default columns and default rows, ie no update by the user.

## Testing on dev v5.4
### Test environment

```json
{
  "url": "http://dev.opensee.ninja:8080",
  "user": "guillaume",
  "creationTime": "Mon Jul 12 11:34:38 2021",
  "runTime": "Thu Jul 15 10:48:04 2021"
}
```
Nb of reference calculators tested : 40
- success : 13 (33%)
- failure : 27 (67%)


### Success calculators (13)

| # | name                    | module           | metricsNb | nbDims | sources                 | distributedOn | failedCheck | MetricsOk | DimsOk | SchemaOk | Status | Error | ErrorType | recordsNb | duration   |
|----------|-------------------------|------------------|-----------|--------|-------------------------|---------------|-------------|-----------|--------|----------|--------|-------|-----------|-----------|------------|
| 2        | CVaR_CPTY_drillBy       | mtm              | 0         | 4      | ['mtms',   'datearray'] | counterparty  | 0           | True      | True   | False    | 200    | False |           | 3168      | 24.7541189 |
| 4        | Projection              | HistoVaR         | 0         | 13     | ['Position_Sensi_WIP']  |               | 0           | True      | True   | False    | 200    | False |           | 2         | 0.69419885 |
| 17       | FRTB_CS                 | sensi_dynamic    | 0         | 3      | ['Sensi']               |               | 0           | True      | True   | False    | 200    | False |           | 10        | 274.566666 |
| 18       | Projection              | sensi_dynamic    | 0         | 36     | ['Sensi']               |               | 0           | True      | False  | False    | 200    | False |           | 5         | 286.257972 |
| 19       | test_subtotals          | sensi_dynamic    | 0         | 6      | ['Sensi']               |               | 0           | True      | True   | False    | 200    | False |           | 1216      | 275.742145 |
| 20       | FRTB_GIRR_Sensi         | sensi_dynamic    | 0         | 4      | ['Sensi']               |               | 0           | True      | True   | False    | 200    | False |           | 4         | 287.315962 |
| 21       | test_multi_metriques    | sensi_dynamic    | 3         | 6      | ['Sensi']               |               | 0           | True      | True   | False    | 200    | False |           | 1268      | 283.5338   |
| 28       | FRTB_CS                 | sensi_nv         | 0         | 2      | ['Sensi_nv']            |               | 0           | True      | True   | False    | 200    | False |           | 10        | 285.667816 |
| 31       | FRTB_GIRR_Sensi         | sensi_nv_dynamic | 0         | 4      | ['Sensi_nv']            |               | 0           | True      | True   | False    | 200    | False |           | 4         | 274.201787 |
| 32       | FRTB_CS                 | sensi_nv_dynamic | 0         | 3      | ['Sensi_nv']            |               | 0           | True      | True   | False    | 200    | False |           | 10        | 294.167794 |
| 34       | test_multi_metriques    | sensi_nv_dynamic | 3         | 6      | ['Sensi_nv']            |               | 0           | True      | True   | False    | 200    | False |           | 1268      | 188.670356 |
| 36       | Projection              | sensi_nv_dynamic | 0         | 36     | ['Sensi_nv']            |               | 0           | True      | False  | False    | 200    | False |           | 5         | 276.018928 |
| 37       | testNullable_calculator | testNullable     | 0         | 4      | ['TestNullable']        |               | 0           | True      | True   | False    | 200    | False |           | 5         | 293.883857 |

On these calculators, none are compliant with the Calculators guideline schema which is required to be compatible with `xmla`.
But all are compliant with the metric definition requirement and only 2 are non compliant with the dimensions definition requirement.

### Failed calculators (27)

| # | name                     | module           | metricsNb | nbDims | sources                                                        | distributedOn | failedCheck | MetricsOk | DimsOk | SchemaOk | Status | Error | ErrorType                  | recordsNb | duration   |
|----------|--------------------------|------------------|-----------|--------|----------------------------------------------------------------|---------------|-------------|-----------|--------|----------|--------|-------|----------------------------|-----------|------------|
| 0        | TestMultiTableCalculator | mtm              | 0         | 4      | ['mtms', 'fxs']                                                | Date          | 0           | False     | False  | False    | 200    | True  | DistributionSetNotMatching | 0         | 0.29285789 |
| 1        | CVaR_CPTY                | mtm              | 0         | 4      | ['mtms', 'datearray']                                          | counterparty  | 1           | False     | False  | False    | 200    | True  | http                       | 0         | 281.319048 |
| 3        | Wrong_Test_non_dim_req   | mtm              | 0         | 4      | ['mtms',   'mtms']                                             | counterparty  | 2           | False     | False  | False    | 200    | True  | IllegalParameter           | 0         | 0.23169017 |
| 5        | detect_error             | HistoVaR         | 0         | 4      | ['Security_PnL_WIP', 'scenario_array']                         | SecurityId    | 0           | False     | False  | False    | 200    | True  | HttpError                  | 0         | 278.733621 |
| 6        | MaxLossContribution      | HistoVaR         | 1         | 12     | ['Position_PnL_WIP',   'scenario_array']                       | Date          | 0           | False     | False  | False    | 200    | True  | HttpError                  | 0         | 24.6757839 |
| 7        | Metrics                  | HistoVaR         | 3         | 12     | ['Position_PnL_WIP', 'scenario_array']                         | Date          | 0           | False     | False  | False    | 200    | False |                            | 0         | 0.38220286 |
| 8        | ESContribution           | HistoVaR         | 1         | 12     | ['Position_PnL_WIP',   'scenario_array']                       | Date          | 0           | False     | False  | False    | 200    | True  | HttpError                  | 0         | 184.00489  |
| 9        | MetricsV1                | HistoVaR         | 3         | 11     | ['Position_PnL_WIP', 'scenario_array']                         | Date          | 0           | False     | False  | False    | 200    | True  | HttpError                  | 0         | 183.730648 |
| 10       | MetricsV1WithSensi       | HistoVaR         | 4         | 12     | ['Position_PnL_WIP',   'scenario_array', 'Position_Sensi_WIP'] | Date          | 0           | False     | False  | False    | 200    | False |                            | 0         | 0.45157671 |
| 11       | Map                      | sensi            | 0         | 36     | ['Sensi']                                                      |               | 0           | False     | False  | False    | 200    | True  | http                       | 0         | 285.989804 |
| 12       | Test                     | sensi            | 0         | 2      | ['Sensi']                                                      |               | 0           | False     | False  | False    | 200    | True  | ClientPayloadError         | 0         | 0.39267755 |
| 13       | FRTB_EQ                  | sensi            | 0         | 3      | ['Sensi']                                                      | Desk          | 1           | False     | False  | False    | 400    | True  | InvalidRequestType         | 0         | 1.38E-05   |
| 14       | EQ_Vol_AdvStressTest     | sensi            | 0         | 3      | ['Sensi']                                                      | Desk          | 1           | False     | False  | False    | 200    | True  | http                       | 0         | 286.697314 |
| 15       | FRTB_GIRR_Sensi          | sensi            | 0         | 3      | ['Sensi']                                                      |               | 0           | False     | False  | False    | 200    | True  | http                       | 0         | 288.149266 |
| 16       | FRTB_CS                  | sensi            | 0         | 2      | ['Sensi']                                                      |               | 0           | False     | False  | False    | 200    | True  | http                       | 0         | 287.786381 |
| 22       | EQ_Vol_AdvStressTest     | sensi_dynamic    | 0         | 3      | ['Sensi']                                                      | Desk          | 1           | False     | False  | False    | 200    | True  | http                       | 0         | 290.698237 |
| 23       | FRTB_EQ                  | sensi_dynamic    | 0         | 3      | ['Sensi']                                                      | Desk          | 1           | False     | False  | False    | 400    | True  | InvalidRequestType         | 0         | 1.65E-05   |
| 24       | FRTB_GIRR_Sensi          | sensi_nv         | 0         | 3      | ['Sensi_nv']                                                   |               | 0           | False     | False  | False    | 200    | True  | http                       | 0         | 291.857616 |
| 25       | EQ_Vol_AdvStressTest     | sensi_nv         | 0         | 3      | ['Sensi_nv']                                                   | Desk          | 1           | False     | False  | False    | 200    | True  | http                       | 0         | 292.640676 |
| 26       | Map                      | sensi_nv         | 0         | 36     | ['Sensi_nv']                                                   |               | 0           | False     | False  | False    | 200    | True  | HttpError                  | 0         | 287.091512 |
| 27       | Test                     | sensi_nv         | 0         | 2      | ['Sensi_nv']                                                   |               | 0           | False     | False  | False    | 200    | True  | ClientPayloadError         | 0         | 0.10101509 |
| 29       | FRTB_EQ                  | sensi_nv         | 0         | 3      | ['Sensi_nv']                                                   | Desk          | 1           | False     | False  | False    | 400    | True  | InvalidRequestType         | 0         | 1.34E-05   |
| 30       | FRTB_EQ                  | sensi_nv_dynamic | 0         | 3      | ['Sensi_nv']                                                   | Desk          | 1           | False     | False  | False    | 400    | True  | InvalidRequestType         | 0         | 2.46E-05   |
| 33       | EQ_Vol_AdvStressTest     | sensi_nv_dynamic | 0         | 3      | ['Sensi_nv']                                                   | Desk          | 1           | False     | False  | False    | 200    | True  | http                       | 0         | 296.714807 |
| 35       | test_subtotals           | sensi_nv_dynamic | 0         | 6      | ['Sensi_nv']                                                   |               | 0           | False     | False  | False    | 200    | True  | http                       | 0         | 297.757611 |
| 38       | CVaR_CPTY_drillBy        | mtm_nv           | 0         | 4      | ['mtms_nv', 'datearray']                                       | counterparty  | 0           | False     | False  | False    | 200    | True  | HttpError                  | 0         | 297.443402 |
| 39       | CVaR_CPTY                | mtm_nv           | 0         | 4      | ['mtms_nv',   'datearray']                                     | counterparty  | 1           | False     | False  | False    | 200    | True  | http                       | 0         | 299.892143 |

- 27 have failed to the tests, out of which 25 have error in the response and 2 have empty answers.

- 4 have invalid http codes. That means that 21 (27-2-4) are responding with wrongly success status despite the failure.

- 6 are returning wrong error type `httpError`, whereas they are showing PyCalc error, and so the message is confusing.

- 2 request displays `ClientPayLoad` error which usually means that the server has unexpectedly closes the connection without finishing (or starting in our case) data transfers.

- 11 requests have timed out after 300 seconds without having answered anything

- 1 have shown an "illegal parameter" error type and more precisely : "Illegal parameter: unable to parse function=<> because '' position: 0"

