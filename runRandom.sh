#!/bin/bash

if [ -z "$1" ]; then
    echo "You must specify a calculator name as first argument"
    echo "arguments : <calc_name> [<output_dir>] [<replay_args> ...]"
else
    calc="$1"
    version_short=$(omb info | awk '/version/ {gsub(/"/,"");gsub(/,/,"");print $2}' | cut -c1-8)
    today="$(date '+%F')"
    report_dir="${2:-random}/${version_short}/${server}/${today}/${calc}"
    file="${report_dir}/${server}_${today}_calcs.json"
    report="${report_dir}/${server}_${today}_calcs_report.json"
    csv="${report_dir}/${server}_${today}_calcs_stats.csv"

    mkdir -p "${report_dir}"

    shift 2
    python testCalc/seq_all_calc.py -o ${file} --calculator ${calc} $*

    # We shift parameters to send all parameter to omb reqs replay except the first one

    omb reqs replay --elapse 1 ${file}

    python testCalc/stats_calculator.py ${report} > ${csv}

    python testCalc/concat_csv.py 
fi